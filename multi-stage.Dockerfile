FROM maven:3-amazoncorretto-17 AS builder
ADD . ./
RUN ./build-linux.sh

FROM registry.gitlab.com/ldath-core/docker/java:17-alpine-jdk
COPY --from=builder target/*.jar /srv/
COPY docker-entrypoint.sh /

EXPOSE 8080

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["java", "-jar", "$PROJECT_JAR_PATH"]
