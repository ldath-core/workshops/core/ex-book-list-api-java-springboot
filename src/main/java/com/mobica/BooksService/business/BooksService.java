package com.mobica.BooksService.business;

import com.mobica.BooksService.BookModelMapper;
import com.mobica.BooksService.network.api.BooksApiDelegate;
import com.mobica.BooksService.network.model.*;
import com.mobica.BooksService.persistence.Book;
import com.mobica.BooksService.persistence.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service public class BooksService implements BooksApiDelegate {
    private final BookRepository bookRepository;

    @Autowired
    public BooksService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public ResponseEntity<BooksOutput> getBooks(Long skip, Long limit) {
        Collection<Book> allBooks = bookRepository.findAllBooks();

        PaginatedBooksContent paginatedBooksContent =
                new PaginatedBooksContent();

        allBooks.forEach(book -> {
            paginatedBooksContent.addResultsItem(BookModelMapper.fromBook(book));
        });
        BooksOutput booksOutput = new BooksOutput();
        booksOutput.setContent(paginatedBooksContent);
        return new ResponseEntity<>(booksOutput, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Success> deleteBookById(Integer id) {
        int rowsAffected = bookRepository.deleteBookById(Long.valueOf(id));
        return rowsAffected > 0 ? new ResponseEntity<>(HttpStatus.OK): new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @Override
    public ResponseEntity<BookOutput> getBookById(Integer id) {
        Book book = bookRepository.findBooksById(id.longValue());
        BookOutput bookOutput = new BookOutput().content(BookModelMapper.fromBook(book));
        return new ResponseEntity<>(bookOutput, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<CreateBookOutput> createBook(BookObject body) {
        Book bookToAdd = BookModelMapper.fromBookObject(body);
        int rowsAffected =  bookRepository.createBook(bookToAdd.getAuthor(), bookToAdd.getTitle(),
        bookToAdd.getDescription(), bookToAdd.getYear(), bookToAdd.getImageURL(), bookToAdd.getImageBase64());
        return rowsAffected > 0 ? new ResponseEntity<>(HttpStatus.CREATED): new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @Override
    public ResponseEntity<BookOutput> putBookById(Integer id, BookObject body) {
        int rowsAffected =  bookRepository.updateBook(id.longValue(), body.getAuthor(), body.getTitle(),
                body.getDescription(), body.getYear(), body.getImage().getUrl(), body.getImage().getBase64());
        return rowsAffected > 0 ? new ResponseEntity<>(HttpStatus.OK): new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
