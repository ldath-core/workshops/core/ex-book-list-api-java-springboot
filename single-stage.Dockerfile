FROM registry.gitlab.com/ldath-core/docker/java:17-alpine-jdk

COPY target/*.jar /srv/
COPY docker-entrypoint.sh /

EXPOSE 8080

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["java" "-jar /srv/BookService*.jar]
