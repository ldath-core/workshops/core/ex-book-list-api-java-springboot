package com.mobica.BooksService.persistence;

import jakarta.persistence.*;

@Entity
@Table
public class Book {

    public Book(){
    }

    public Book(String title, String author, String year, String description, String imageURL, String imageBase64) {
        this.title = title;
        this.author = author;
        this.year = year;
        this.description = description;
        this.imageURL = imageURL;
        this.imageBase64 = imageBase64;
    }

    @Id
    @SequenceGenerator(
            name = "book_sequence",
            sequenceName = "book_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "book_sequence"
    )
    private Long id;
    private String title;
    private String author;
    private String year;
    private String description;
    private String imageURL;
    private String imageBase64;

    public String getAuthor() {
        return author;
    }

    public String getDescription() {
        return description;
    }

    public String getYear() {
        return year;
    }

    public String getTitle() {
        return title;
    }

    public Long getId() {
        return id;
    }

    public String getImageURL() {
        return imageURL;
    }

    public String getImageBase64() {
        return imageBase64;
    }
}
