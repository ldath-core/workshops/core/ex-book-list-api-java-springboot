package com.mobica.BooksService.network.api;

import com.mobica.BooksService.network.model.BookObject;
import com.mobica.BooksService.network.model.BookOutput;
import com.mobica.BooksService.network.model.BooksOutput;
import com.mobica.BooksService.network.model.CreateBookOutput;
import com.mobica.BooksService.network.model.Error;
import com.mobica.BooksService.network.model.Success;
import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * A delegate to be called by the {@link BooksApiController}}.
 * Implement this interface with a {@link org.springframework.stereotype.Service} annotated class.
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-03-29T18:36:40.009568300+02:00[Europe/Warsaw]")
public interface BooksApiDelegate {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    /**
     * POST /books : Create Book
     * This endpoint creating new book-list and is sending details of book-list with assigned ID 
     *
     * @param body Creating book-list (required)
     * @return Success response for the book-list creation request (status code 201)
     *         or Unexpected error (status code 200)
     * @see BooksApi#createBook
     */
    default ResponseEntity<CreateBookOutput> createBook(BookObject body) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "{ \"message\" : \"message\", \"content\" : { \"id\" : 6 }, \"status\" : 0 }";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    /**
     * DELETE /books/{id} : Delete Book by ID
     * This endpoint is deleting book-list 
     *
     * @param id The ID of an book-list (required)
     * @return Default Success response (status code 202)
     *         or Not Found (status code 404)
     *         or Unexpected error (status code 200)
     * @see BooksApi#deleteBookById
     */
    default ResponseEntity<Success> deleteBookById(Integer id) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "{ \"message\" : \"message\", \"content\" : \"{}\", \"status\" : 0 }";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    /**
     * GET /books/{id} : Get Book by ID
     * This endpoint is sending details of book-list chosen by ID 
     *
     * @param id The ID of an book-list (required)
     * @return Success response for the book-list request (status code 200)
     *         or Not Found (status code 404)
     *         or Unexpected error (status code 200)
     * @see BooksApi#getBookById
     */
    default ResponseEntity<BookOutput> getBookById(Integer id) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "{ \"message\" : \"message\", \"content\" : { \"image\" : { \"base64\" : \"base64\", \"url\" : \"url\" }, \"year\" : \"year\", \"author\" : \"author\", \"description\" : \"description\", \"id\" : 6, \"title\" : \"title\" }, \"status\" : 0 }";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    /**
     * GET /books : Get Books
     * This endpoint is sending details of all book-lists 
     *
     * @param skip how many records to skip (optional, default to 0)
     * @param limit how many records to return (optional, default to 10)
     * @return Success response for the book-lists request (status code 200)
     *         or Unexpected error (status code 200)
     * @see BooksApi#getBooks
     */
    default ResponseEntity<BooksOutput> getBooks(Long skip,
        Long limit) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "{ \"message\" : \"message\", \"content\" : { \"count\" : 42, \"limit\" : 10, \"skip\" : 0, \"results\" : [ { \"image\" : { \"base64\" : \"base64\", \"url\" : \"url\" }, \"year\" : \"year\", \"author\" : \"author\", \"description\" : \"description\", \"id\" : 6, \"title\" : \"title\" }, { \"image\" : { \"base64\" : \"base64\", \"url\" : \"url\" }, \"year\" : \"year\", \"author\" : \"author\", \"description\" : \"description\", \"id\" : 6, \"title\" : \"title\" } ] }, \"status\" : 0 }";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    /**
     * PUT /books/{id} : Put Book by ID
     * This endpoint is updating book-list 
     *
     * @param id The ID of an book-list (required)
     * @param body Updating book-list (required)
     * @return Success response for the book-list request (status code 202)
     *         or Not Found (status code 404)
     *         or Unexpected error (status code 200)
     * @see BooksApi#putBookById
     */
    default ResponseEntity<BookOutput> putBookById(Integer id,
        BookObject body) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "{ \"message\" : \"message\", \"content\" : { \"image\" : { \"base64\" : \"base64\", \"url\" : \"url\" }, \"year\" : \"year\", \"author\" : \"author\", \"description\" : \"description\", \"id\" : 6, \"title\" : \"title\" }, \"status\" : 0 }";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

}
