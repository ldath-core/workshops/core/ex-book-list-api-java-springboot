package com.mobica.BooksService.network.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.mobica.BooksService.network.model.BookObject;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * PaginatedBooksContent
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-03-29T18:36:40.009568300+02:00[Europe/Warsaw]")
public class PaginatedBooksContent   {
  @JsonProperty("count")
  private Integer count;

  @JsonProperty("skip")
  private Integer skip = 0;

  @JsonProperty("limit")
  private Integer limit = 10;

  @JsonProperty("results")
  @Valid
  private List<BookObject> results = new ArrayList<>();

  public PaginatedBooksContent count(Integer count) {
    this.count = count;
    return this;
  }

  /**
   * Get count
   * @return count
  */
  @ApiModelProperty(example = "42", required = true, value = "")
  @NotNull


  public Integer getCount() {
    return count;
  }

  public void setCount(Integer count) {
    this.count = count;
  }

  public PaginatedBooksContent skip(Integer skip) {
    this.skip = skip;
    return this;
  }

  /**
   * Get skip
   * @return skip
  */
  @ApiModelProperty(example = "0", required = true, value = "")
  @NotNull


  public Integer getSkip() {
    return skip;
  }

  public void setSkip(Integer skip) {
    this.skip = skip;
  }

  public PaginatedBooksContent limit(Integer limit) {
    this.limit = limit;
    return this;
  }

  /**
   * Get limit
   * @return limit
  */
  @ApiModelProperty(example = "10", required = true, value = "")
  @NotNull


  public Integer getLimit() {
    return limit;
  }

  public void setLimit(Integer limit) {
    this.limit = limit;
  }

  public PaginatedBooksContent results(List<BookObject> results) {
    this.results = results;
    return this;
  }

  public PaginatedBooksContent addResultsItem(BookObject resultsItem) {
    if (this.results == null) {
      this.results = new ArrayList<>();
    }
    this.results.add(resultsItem);
    return this;
  }

  /**
   * Get results
   * @return results
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public List<BookObject> getResults() {
    return results;
  }

  public void setResults(List<BookObject> results) {
    this.results = results;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PaginatedBooksContent paginatedBooksContent = (PaginatedBooksContent) o;
    return Objects.equals(this.count, paginatedBooksContent.count) &&
        Objects.equals(this.skip, paginatedBooksContent.skip) &&
        Objects.equals(this.limit, paginatedBooksContent.limit) &&
        Objects.equals(this.results, paginatedBooksContent.results);
  }

  @Override
  public int hashCode() {
    return Objects.hash(count, skip, limit, results);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PaginatedBooksContent {\n");
    
    sb.append("    count: ").append(toIndentedString(count)).append("\n");
    sb.append("    skip: ").append(toIndentedString(skip)).append("\n");
    sb.append("    limit: ").append(toIndentedString(limit)).append("\n");
    sb.append("    results: ").append(toIndentedString(results)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

