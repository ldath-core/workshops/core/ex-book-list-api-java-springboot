package com.mobica.BooksService.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@Transactional
public interface BookRepository extends JpaRepository<Book, Long> {

    @Query("SELECT b FROM Book b")
    public Collection<Book> findAllBooks();

    @Query("SELECT b FROM Book b WHERE b.id = (:id)")
    public Book findBooksById(@Param("id") Long Id);

    @Query("DELETE FROM Book WHERE id = (:id)")
    @Modifying
    public int deleteBookById(@Param("id") Long Id);

    @Modifying
    @Query("INSERT INTO Book (author, title, description, year, imageURL, imageBase64) VALUES (:author, :title, :description, :year, :imageURL, :imageBase64)")
    public int createBook(@Param("author") String author, @Param("title") String title,
                          @Param("description") String description, @Param("year") String year,
                          @Param("imageURL") String imageURL, @Param("imageBase64") String imageBase64
    );

    @Modifying
    @Query("UPDATE Book b SET b.author = (:author), b.title = (:title), b.description = (:description), b.year = (:year), b.imageURL = (:imageURL), b.imageBase64 = (:imageBase64) WHERE b.id = (:id)")
    public int updateBook(@Param("id") Long Id, @Param("author") String author, @Param("title") String title,
                          @Param("description") String description, @Param("year") String year,
                          @Param("imageURL") String imageURL, @Param("imageBase64") String imageBase64);
}
