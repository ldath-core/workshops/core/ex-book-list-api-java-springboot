#!/usr/bin/env bash
set -e
mvn -ntp -q clean
mvn -ntp -q install
