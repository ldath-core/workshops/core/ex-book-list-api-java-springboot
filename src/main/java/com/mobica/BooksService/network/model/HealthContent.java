package com.mobica.BooksService.network.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * HealthContent
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-03-29T18:36:40.009568300+02:00[Europe/Warsaw]")
public class HealthContent   {
  @JsonProperty("alive")
  private Boolean alive;

  @JsonProperty("postgres")
  private Boolean postgres;

  public HealthContent alive(Boolean alive) {
    this.alive = alive;
    return this;
  }

  /**
   * Get alive
   * @return alive
  */
  @ApiModelProperty(value = "")


  public Boolean getAlive() {
    return alive;
  }

  public void setAlive(Boolean alive) {
    this.alive = alive;
  }

  public HealthContent postgres(Boolean postgres) {
    this.postgres = postgres;
    return this;
  }

  /**
   * Get postgres
   * @return postgres
  */
  @ApiModelProperty(value = "")


  public Boolean getPostgres() {
    return postgres;
  }

  public void setPostgres(Boolean postgres) {
    this.postgres = postgres;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    HealthContent healthContent = (HealthContent) o;
    return Objects.equals(this.alive, healthContent.alive) &&
        Objects.equals(this.postgres, healthContent.postgres);
  }

  @Override
  public int hashCode() {
    return Objects.hash(alive, postgres);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class HealthContent {\n");
    
    sb.append("    alive: ").append(toIndentedString(alive)).append("\n");
    sb.append("    postgres: ").append(toIndentedString(postgres)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

