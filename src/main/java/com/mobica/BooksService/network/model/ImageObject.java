package com.mobica.BooksService.network.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ImageObject
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-03-29T18:36:40.009568300+02:00[Europe/Warsaw]")
public class ImageObject   {
  @JsonProperty("base64")
  private String base64 = null;

  @JsonProperty("url")
  private String url = null;

  public ImageObject base64(String base64) {
    this.base64 = base64;
    return this;
  }

  /**
   * Get base64
   * @return base64
  */
  @ApiModelProperty(value = "")


  public String getBase64() {
    return base64;
  }

  public void setBase64(String base64) {
    this.base64 = base64;
  }

  public ImageObject url(String url) {
    this.url = url;
    return this;
  }

  /**
   * Get url
   * @return url
  */
  @ApiModelProperty(value = "")


  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ImageObject imageObject = (ImageObject) o;
    return Objects.equals(this.base64, imageObject.base64) &&
        Objects.equals(this.url, imageObject.url);
  }

  @Override
  public int hashCode() {
    return Objects.hash(base64, url);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ImageObject {\n");
    
    sb.append("    base64: ").append(toIndentedString(base64)).append("\n");
    sb.append("    url: ").append(toIndentedString(url)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

