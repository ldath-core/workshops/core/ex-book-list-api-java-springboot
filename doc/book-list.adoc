= Book List API
Arkadiusz Tułodziecki <atulodzi@gmail.com>
1.0, Aug 31, 2022: AsciiDoc article template
:toc:
:source-highlighter: rouge
:encoding: utf-8
:icons: font
:url-quickref: https://docs.asciidoctor.org/asciidoc/latest/syntax-quick-reference/

ifdef::backend-html5[]
PDF version of this article:
link:book-list.pdf/[book-list.pdf]
endif::[]

== Local Development Environment

This documentation shows how to create local development environment on Linux Desktop or macOS.

This projects needs to be cloned into `~/go/src/gitlab.com/mobica-workshops/examples/go/gorilla` folder in case
when `$GOPATH` is set to `~/go`.

=== Tools

Those tools need to be installed prior to configuring a project to work.

This projects needs to be cloned into `~/go/src/gitlab.com/mobica-workshops/examples/go/gorilla` folder in case
when `$GOPATH` is set to `~/go`.

This Project requires those tools for the development:

- **Brew** - optional
- **Docker Engine** - required
- **Docker Compose** - required
- **Ansible** - required
- **Helm** - optional
- **K3d** - optional
- **Trivy** - optional
- **Grype** - optional

and `Go` installed and configured

You can find how to install required tools for this service here: https://mobica-workshops.gitlab.io/documentation/requirements-guide/

=== Configuring project

include::configuring.adoc[]

=== Development

==== Service Development and Continuous Integration

include::development-service.adoc[]

==== Continuous Delivery and end-to-end testing

include::development-dockerfile.adoc[]

==== Continuous Deployment Testing

include::development-kubernetes.adoc[]

image:https://i.creativecommons.org/l/by/4.0/88x31.png[Creative Commons Licence,88,31] This work is licensed under a http://creativecommons.org/licenses/by/4.0/[Creative Commons Attribution 4.0 International License]
