package com.mobica.BooksService;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mobica.BooksService.network.model.BookObject;
import com.mobica.BooksService.network.model.ImageObject;
import com.mobica.BooksService.persistence.Book;
import org.openapitools.jackson.nullable.JsonNullableModule;

public class BookModelMapper {
    public static Book fromBookObject(BookObject bookObject){
        return new Book(bookObject.getTitle(),
                bookObject.getAuthor(), bookObject.getYear(),
                bookObject.getDescription(), bookObject.getImage().getBase64(),
                bookObject.getImage().getUrl());
    }

    public static BookObject fromBook(Book book){
        BookObject bookObject = new BookObject();
        bookObject.setId(book.getId().intValue());
        bookObject.setTitle(book.getTitle());
        bookObject.setAuthor(book.getAuthor());
        bookObject.setYear(book.getYear());
        bookObject.setDescription( book.getDescription());
        ImageObject imageObject = new ImageObject();
        imageObject.setUrl( book.getImageURL());
        imageObject.setBase64( book.getImageBase64());
        bookObject.setImage(imageObject);
        return bookObject;
    }
}

//     public Book(Long id, String title, String author, String year, String description, String url, String base64) {
