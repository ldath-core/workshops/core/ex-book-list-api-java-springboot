openapi: 3.0.0
servers:
  - description: Development API endpoint
    url: 'http://localhost:8882/v1'
  - description: Docker API endpoint
    url: 'http://localhost:8882/v1'
  - description: Private API endpoint
    url: 'http://ex-book-list-api-go/v1'
  - description: Development API Gateway endpoint
    url: 'http://localhost:8881/book-list/v1'

info:
  description: This is a simple API
  version: "1.0.0"
  title: ex-book-list-api-go
  contact:
    email: atulodzi@gmail.com
  license:
    name: Apache 2.0
    url: 'https://www.apache.org/licenses/LICENSE-2.0.html'

tags:
  - name: health
    description: Health endpoints
  - name: book-list
    description: Book related endpoints

paths:

  ###############
  # health path #
  ###############

  '/health':
    get:
      tags:
        - health
      security:
        - {}
        - jwtAuth: []
      summary: Get Health
      operationId: getHealth
      description: |
        This endpoint is sending health information
      responses:
        '200':
          $ref: '#/components/responses/HealthResponse'
        '500':
          $ref: '#/components/responses/InternalServerErrorResponse'
        '501':
          $ref: '#/components/responses/NotImplementedResponse'

  ####################
  # book-list paths #
  ####################

  '/books':
    post:
      tags:
        - book-list
      security:
        - {}
        - jwtAuth: []
      summary: Create Book
      operationId: createBook
      description: |
        This endpoint creating new book-list and is sending details of book-list with assigned ID
      requestBody:
        $ref: '#/components/requestBodies/CreateBookRequest'
      responses:
        '201':
          $ref: '#/components/responses/CreateBookResponse'
        default:
          $ref: '#/components/responses/DefaultErrorResponse'
    get:
      tags:
        - book-list
      security:
        - {}
        - jwtAuth: []
      summary: Get Books
      operationId: getBooks
      description: |
        This endpoint is sending details of all book-lists
      parameters:
        - in: query
          name: skip
          description: how many records to skip
          required: false
          schema:
            type: integer
            format: int64
            minimum: 0
            default: 0
        - in: query
          name: limit
          description: how many records to return
          required: false
          schema:
            type: integer
            format: int64
            minimum: 1
            default: 10
      responses:
        '200':
          $ref: '#/components/responses/BooksResponse'
        default:
          $ref: '#/components/responses/DefaultErrorResponse'

  '/books/{id}':
    get:
      tags:
        - book-list
      security:
        - {}
        - jwtAuth: []
      summary: Get Book by ID
      operationId: getBookById
      description: |
        This endpoint is sending details of book-list chosen by ID
      parameters:
        - in: path
          name: id
          description: 'The ID of an book-list'
          required: true
          schema:
            type: integer
      responses:
        '200':
          $ref: '#/components/responses/BookResponse'
        '404':
          $ref: '#/components/responses/NotFoundErrorResponse'
        default:
          $ref: '#/components/responses/DefaultErrorResponse'
    put:
      tags:
        - book-list
      security:
        - {}
        - jwtAuth: []
      summary: Put Book by ID
      operationId: putBookById
      description: |
        This endpoint is updating book-list
      parameters:
        - in: path
          name: id
          description: 'The ID of an book-list'
          required: true
          schema:
            type: integer
      requestBody:
        $ref: '#/components/requestBodies/UpdateBookRequest'
      responses:
        '202':
          $ref: '#/components/responses/BookResponse'
        '404':
          $ref: '#/components/responses/NotFoundErrorResponse'
        default:
          $ref: '#/components/responses/DefaultErrorResponse'
    delete:
      tags:
        - book-list
      security:
        - {}
        - jwtAuth: []
      summary: Delete Book by ID
      operationId: deleteBookById
      description: |
        This endpoint is deleting book-list
      parameters:
        - in: path
          name: id
          description: 'The ID of an book-list'
          required: true
          schema:
            type: integer
      responses:
        '202':
          $ref: '#/components/responses/SuccessResponse'
        '404':
          $ref: '#/components/responses/NotFoundErrorResponse'
        default:
          $ref: '#/components/responses/DefaultErrorResponse'

components:

  requestBodies:

    CreateBookRequest:
      description: Creating book-list
      required: true
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/CreateBookInput'
    UpdateBookRequest:
      description: Updating book-list
      required: true
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/UpdateBookInput'

  responses:

    ######################
    # standard responses #
    ######################

    SuccessResponse:
      description: 'Default Success response'
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Success'

    NotImplementedResponse:
      description: Not Implemented
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Error'

    InternalServerErrorResponse:
      description: Internal Server Error
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Error'

    NotFoundErrorResponse:
      description: Not Found
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Error'

    DefaultErrorResponse:
      description: Unexpected error
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Error'

    ###################
    # health response #
    ###################

    HealthResponse:
      description: 'Health information'
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Health'

    ########################
    # book-list responses #
    ########################

    CreateBookResponse:
      description: 'Success response for the book-list creation request'
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/CreateBookOutput'

    BooksResponse:
      description: 'Success response for the book-lists request'
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/BooksOutput'

    BookResponse:
      description: 'Success response for the book-list request'
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/BookOutput'

  schemas:

    ####################
    # standard objects #
    ####################

    Success:
      type: object
      properties:
        status:
          type: integer
          default: 200
        message:
          type: string
        content:
          type: object
          nullable: true

    Error:
      type: object
      properties:
        status:
          type: integer
          example: 404
        message:
          type: string
          example: "book-list not found"
        errors:
          type: array
          items:
            type: string
          nullable: true

    ##################
    # health objects #
    ##################

    Health:
      type: object
      properties:
        status:
          type: integer
          default: 200
        message:
          type: string
          default: "book book api health"
        content:
          $ref: '#/components/schemas/HealthContent'

    HealthContent:
      type: object
      properties:
        alive:
          type: boolean
        postgres:
          type: boolean

    ######################
    # book-list objects #
    ######################

    CreateBookInput:
      $ref: '#/components/schemas/BookObject'

    CreateBookOutput:
      type: object
      properties:
        status:
          type: integer
          default: 201
        message:
          type: string
        content:
          $ref: '#/components/schemas/CreatedBook'

    CreatedBook:
      type: object
      properties:
        id:
          type: integer
          description: "id of the created object"

    UpdateBookInput:
      $ref: '#/components/schemas/BookObject'

    BooksOutput:
      type: object
      required:
        - status
        - message
        - content
      properties:
        status:
          type: integer
          default: 200
        message:
          type: string
        content:
          $ref: '#/components/schemas/PaginatedBooksContent'
    PaginatedBooksContent:
      type: object
      required:
        - count
        - skip
        - limit
        - results
      properties:
        count:
          type: integer
          example: 42
        skip:
          type: integer
          example: 0
          default: 0
        limit:
          type: integer
          example: 10
          default: 10
        results:
          type: array
          items:
            $ref: '#/components/schemas/BookObject'
    BookOutput:
      type: object
      required:
        - status
        - message
        - content
      properties:
        status:
          type: integer
          default: 200
        message:
          type: string
        content:
          $ref: '#/components/schemas/BookObject'
    BookObject:
      type: object
      required:
        - id
        - title
        - author
        - year
      properties:
        id:
          type: integer
          description: ID of the Book in the Database
          readOnly: true
        title:
          type: string
          nullable: false
        author:
          type: string
          nullable: false
        year:
          type: string
          nullable: false
        description:
          type: string
          nullable: true
        image:
          $ref: '#/components/schemas/ImageObject'
    ImageObject:
      type: object
      properties:
        base64:
          type: string
          nullable: true
        url:
          type: string
          nullable: true

  securitySchemes:
    jwtAuth:
      type: http
      scheme: bearer
      bearerFormat: JWT    # optional, arbitrary value for documentation purposes

security:
  - jwtAuth: []
